#!/bin/bash

source "../lib/handle_error.sh"

trap 'handle_error $? $LINENO' ERR INT

apt update

apt install -y apache2

service apache2 start
