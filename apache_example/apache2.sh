#!/bin/bash -ex

apt update
if [ "$?" -ne "0" ]
then
    echo "Unable to update repository information"
    exit 1
fi

apt install -y apache2
if [ "$?" -ne "0" ]
then
    echo "Unable to install the apache2 package"
    exit 1
fi

service apache2 start
if [ "$?" -ne "0" ]
then
    echo "Unable to start apache2"
    exit 1
fi

