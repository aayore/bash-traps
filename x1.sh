#!/bin/bash

# Enable the "exit on error" option
# a.k.a "-e"
set -o errexit

# Display each script command on the screen before executing it
# a.k.a. "-x"
set -o xtrace

# This command really ought to be successful...
ls

# And this command should fail:
touch does_not_exist/testfile

# Run this if we reach the end:
echo "How did we reach the end?"