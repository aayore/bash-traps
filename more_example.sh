#!/bin/bash

# Enable the "exit on error" option
# set -o errexit

# Enable inheritance of ERR traps
set -o errtrace


# trap "echo Not by the hair of my chinny chin chin!" SIGINT SIGQUIT SIGTERM
# trap "echo Build a house of bricks; exit" SIGINT
# trap 'exit_status=$?;
#         echo >&2 "Error - exited with status ${exit_status} at line $LINENO:";
#         pr -tn $0 | tail -n+$((LINENO - 2)) | head -n4;
#         exit ${exit_status}' ERR INT

trap 'exit_status=$?;
    echo >&2 "Error - exited with status ${exit_status}";
    exit ${exit_status}' ERR INT

for i in $(seq 2)
do
    echo Little pig, little pig, let me in!
    sleep 1
done

(
    true
    true
    false
    true
)

echo Build a house of bricks.