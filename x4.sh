#!/bin/bash

# Import/include our error handling function
source "lib/handle_error.sh"

# Configure our trap
trap 'handle_error $? $LINENO' ERR INT

# This command should work almost anywhere
ls

# And this command should fail
touch does_not_exist/testfile

# Run this if we reach the end
echo "How did we reach the end?"