# Bash Traps

![QR](img/qr.png)

## The Problem

I worked with a client who was (rightfully) dissatisfied with the lack of error handling in one of my scripts.

For every line of code...

```shell
touch a/butt
```

...he added something like this:

```shell
touch a/butt
if [ "$?" -ne "0" ]
then
    echo "Unable to touch a/butt"
    exit 1
fi
```

This inflated the script almost 500%.  The difference between a one-line script and a 5-line script isn't huge.  But the difference between a 100-line script and a 500-line script is huge.

In my defense, the script started with `#!/bin/bash -ex`.  You can find the full descriptions of each option in [The Bash Reference Manual](https://www.gnu.org/software/bash/manual/bash.html#The-Set-Builtin), but here are brief descriptions:

- `-e` or `-o errexit`
    ```
    Exit immediately if a line in the script exits with a non-zero status.
    ```
- `-x` or `-o xtrace`
    ```
    Print each command before it is executed.
    ```

These options, in general, mean that you'll see which command failed and the script will stop executing.  The client's addition provided little extra value beyond customizing an error message.

Consider this example:

```shell
#!/bin/bash -ex

apt update

apt install -y apache2

service apache2 start
```

Compared to:

```shell
#!/bin/bash -ex

apt update
if [ "$?" -ne "0" ]
then
    echo "Unable to update repository information"
    exit 1
fi

apt install -y apache2
if [ "$?" -ne "0" ]
then
    echo "Unable to install the apache2 package"
    exit 1
fi

service apache2 start
if [ "$?" -ne "0" ]
then
    echo "Unable to start apache2"
    exit 1
fi
```

You do get custom error messages that can be as nice as you want them.  But (disregarding whitespace) three lines of code is now 18 lines of code.  Readability suffers.  Maintainability suffers, too, because you'll likely want to change the associated error message when you change a command.

---

Let's look at something like the original script:

### <a href="x1.sh" target="_top">Example 1</a>

---

## The Solution: `trap`

I figured there had to be a better way, so I started researching.  And I learned about the [trap](https://media.giphy.com/media/lk0TFUdop2JTW/giphy.gif).

The basic syntax for [trap](https://www.gnu.org/software/bash/manual/bash.html#index-trap) is:

```shell
trap [argument] [sigspec …]
```

The manual says: 

> The commands in **argument** are to be read and executed when the shell receives signal **sigspec**.

Think: "sigspec" === "signal specification"

So in AaronLang:

```shell
trap [what to do] [when to do it]
```

A quick note on [signals](https://www.gnu.org/software/coreutils/manual/html_node/Signal-specifications.html)!

There are a few different standards for signal implementations, but they're all associated with numbers 0-128.  You can use `trap -l` on most [POSIX](https://en.wikipedia.org/wiki/POSIX) (Unix-like) systems to see what's configured locally:

```
 1) SIGHUP       2) SIGINT       3) SIGQUIT      4) SIGILL       5) SIGTRAP
 6) SIGABRT      7) SIGBUS       8) SIGFPE       9) SIGKILL     10) SIGUSR1
11) SIGSEGV     12) SIGUSR2     13) SIGPIPE     14) SIGALRM     15) SIGTERM
...
```

---

### First Draft

Let's try the same script as above with the `trap` command at the start.

### <a href="x2.sh" target="_top">Example 2</a>

So what are we doing?  We're only adding one line, but let's take a look:

```shell
trap 'ec=$?; echo Error!; exit ${ec}' ERR INT
```

Replacing semicolons with line breaks and adding comments, our **argument** is

```shell
# Collect the exit code from the offending line
ec=$?

# Let the user know there was an error
echo Error!

# Exit with the same exit code generated by the offending line
exit ${ec}
```

And our `sigspec` - or _signal specification_ - tells `trap` to operate on `ERR` (error) and `INT` (interrupt) signals.

---

### Improvements!

First, we're going to remove our `errexit` (because _we're_ handling the errors, now!) and `xtrace` (because we'll highlight the offending line) options.

Then we're going to add an error handling function instead of trying to do it all on one line.  This means we update our `trap` command to:

```shell
trap 'handle_error $? $LINENO' ERR INT
```

We're just calling a `handle_error` function and passing the exit status (`$?`) and line number for the source of the trap

The `handle_error` function looks like this:

```shell
handle_error()
{
    # Collect the exit code and line number from the offending line (source of the trap)
    exit_status=$1
    lineno=$2

    # Grab the offending line from our script
    offending_line=$(sed -n -e "${lineno} p" $0)

    # Build an error message
    errmsg="\nERROR: '$0' exited with status ${exit_status} at ${lineno}\n"
    errmsg+="The line causing the failure is:\n"
    errmsg+="\t${offending_line}"

    # Write our error message to STDERR
    echo -e ${errmsg} >&2

    # Exit with the same status as our offending line
    exit ${exit_status}
}
```

This all results in much cleaner output.  Let's take a look:


### <a href="x3.sh" target="_top">Example 3</a>

---

## Error Handling as a Module

Instead of putting our `handle_error()` function inline in all our scripts, we can easily make it a module:

Create a file `lib/handle_error.sh` using the code above, and we can import it to our scripts like this:

```shell
#!/bin/bash

source "lib/handle_error.sh"

trap 'handle_error $? $LINENO' ERR INT

apt update

apt install -y apache2

service apache2 start
```

Personally, I'm more inclined towards better comments if I have a small block of code that will be accessed repeatedly.  When I'm going to put a kinda-irrelevant chunk of code in my script, I almost try to hide it by removing whitespace and omitting comments.

---

## Try it out

If you want to try this and your shell doesn't seem to work as expected (\*cough\* MacOS \*cough\*), running in Docker is an easy way to go:

1. Install Docker
1. Clone this repo
1. `cd <path_to_which_you_cloned_this_repo>`
1. `docker run -it -v $(pwd):/data ubuntu:latest`
1. `cd /data`
1. Run one of the scripts: `./x1.sh`

To clean your environment, just exit the Docker container (ctrl+d), then start back at the `docker run ...` command.