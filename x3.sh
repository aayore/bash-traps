#!/bin/bash

handle_error()
{
    # Collect the exit code and line number from the offending line (source of the trap)
    exit_status=$1
    lineno=$2

    # Grab the offending line from our script
    offending_line=$(sed -n -e "${lineno} p" $0)
    # https://explainshell.com/explain?cmd=offending_line%3D%24%28sed+-n+-e+%22%24%7Blineno%7D+p%22+%240%29

    # Build an error message
    errmsg="\nERROR: '$0' exited with status ${exit_status} at ${lineno}\n"
    errmsg+="The line causing the failure is:\n"
    errmsg+="\t${offending_line}"

    # Write our error message to STDERR
    echo -e ${errmsg} >&2

    # Exit with the same status as our offending line
    exit ${exit_status}
}

# Configure our trap
trap 'handle_error $? $LINENO' ERR INT

# This command should work almost anywhere
ls

# And this command should fail
touch does_not_exist/testfile

# Run this if we reach the end
echo "How did we reach the end?"